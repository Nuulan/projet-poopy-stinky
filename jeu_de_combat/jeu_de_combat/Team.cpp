#include "Team.h"
#include "IsTest.h"

void createTeam(CTeam* t, CLcharacter* Liste)
{
	system("cls");
	int choix1, choix2, choix3, nbInv, i;
	nbInv = Liste->listeCharacter.size();
	for ( i = 0; i < nbInv; i++)
	{
		std::cout << i << " - " << Liste->listeCharacter[i]->getNom() << std::endl;
	}
	std::cout << i << " - Afficher les stats" << std::endl;
	std::cout << "Veuiilez choisir vos trois personnages : \n";
	std::cout << "Choix premier personnage : ";
	while (std::cin >> choix1)
	{
		if (choix1 <= nbInv - 1 && choix1 >= 0)
		{
			t->set(Liste->listeCharacter[choix1]->copy());
			std::cout << "Vous avez selectionner : " << Liste->listeCharacter[choix1]->getNom() << std::endl;
			break;
		}
		if (choix1 == nbInv)
		{
			for (i = 0; i < nbInv; i++)
			{
				std::cout << i << " - " << Liste->listeCharacter[i]->getNom() << std::endl;
				if (IsChara<CMage>(*(Liste->listeCharacter[i])) == true)
				{
					std::cout << "Classe : Mage" << std::endl;
				}
				if (IsChara<CWarrior>(*(Liste->listeCharacter[i])) == true)
				{
					std::cout << "Classe : Guerrier" << std::endl;
				}
				if (IsChara<CArcher>(*(Liste->listeCharacter[i])) == true)
				{
					std::cout << "Classe : Archer" << std::endl;
				}
				if (IsChara<CRogue>(*(Liste->listeCharacter[i])) == true)
				{
					std::cout << "Classe : Voleur" << std::endl;

				}
				std::cout << "HP : "<< Liste->listeCharacter[i]->getVie() << std::endl;
				std::cout << "Attaque : " << Liste->listeCharacter[i]->getAttaque() << std::endl;
				std::cout << "Agilite : " << Liste->listeCharacter[i]->getAgilite() << std::endl;
				std::cout << "Defense : " << Liste->listeCharacter[i]->getDefense() << std::endl;
				std::cout << "Esquive : " << Liste->listeCharacter[i]->getEsquive() << std::endl;
				std::cout << "Intellignece : " << Liste->listeCharacter[i]->getIntelligence() << std::endl;
				std::cout << "Vitesse : " << Liste->listeCharacter[i]->getVitesse() << "\n\n\n";
			}
			std::cout << "Choix premier personnage : ";
		}
		else
		{
			std::cout << "Le choix n'est pas dans la liste des personnage !\n";
			std::cout << "Choix premier personnage : ";
		}
	}
	std::cout << "Choix deuxi�me personnage : ";
	while (std::cin >> choix2)
	{
		if (choix2 <= nbInv - 1 && choix2 >= 0)
		{
			t->set(Liste->listeCharacter[choix2]->copy());
			std::cout << "Vous avez selectionner : " << Liste->listeCharacter[choix2]->getNom() << std::endl;
			break;
		}
		if (choix2 == nbInv)
		{
			for (i = 0; i < nbInv; i++)
			{
				std::cout << i << " - " << Liste->listeCharacter[i]->getNom() << std::endl;
				if (IsChara<CMage>(*(Liste->listeCharacter[i])) == true)
				{
					std::cout << "Classe : Mage" << std::endl;
				}
				if (IsChara<CWarrior>(*(Liste->listeCharacter[i])) == true)
				{
					std::cout << "Classe : Guerrier" << std::endl;
				}
				if (IsChara<CArcher>(*(Liste->listeCharacter[i])) == true)
				{
					std::cout << "Classe : Archer" << std::endl;
				}
				if (IsChara<CRogue>(*(Liste->listeCharacter[i])) == true)
				{
					std::cout << "Classe : Voleur" << std::endl;
				}
				std::cout << "HP : " << Liste->listeCharacter[i]->getVie() << std::endl;
				std::cout << "Attaque : " << Liste->listeCharacter[i]->getAttaque() << std::endl;
				std::cout << "Agilite : " << Liste->listeCharacter[i]->getAgilite() << std::endl;
				std::cout << "Defense : " << Liste->listeCharacter[i]->getDefense() << std::endl;
				std::cout << "Esquive : " << Liste->listeCharacter[i]->getEsquive() << std::endl;
				std::cout << "Intellignece : " << Liste->listeCharacter[i]->getIntelligence() << std::endl;
				std::cout << "Vitesse : " << Liste->listeCharacter[i]->getVitesse() << "\n\n\n";
			}
			std::cout << "Choix deuxi�me personnage : ";
		}
		else
		{
			std::cout << "Le choix n'est pas dans la liste des personnage !\n";
			std::cout << "Choix deuxi�me personnage : ";
		}
	}
	std::cout << "Choix troisi�me personnage : ";
	while (std::cin >> choix3)
	{
		if (choix3 <= nbInv - 1 && choix3 >= 0)
		{
			t->set(Liste->listeCharacter[choix3]->copy());
			std::cout << "Vous avez selectionner : " << Liste->listeCharacter[choix3]->getNom() << std::endl;
			break;
		}
		if (choix3 == nbInv)
		{
			for (i = 0; i < nbInv; i++)
			{
				std::cout << i << " - " << Liste->listeCharacter[i]->getNom() << std::endl;
				if (IsChara<CMage>(*(Liste->listeCharacter[i])) == true)
				{
					std::cout << "Classe : Mage" << std::endl;
				}
				if (IsChara<CWarrior>(*(Liste->listeCharacter[i])) == true)
				{
					std::cout << "Classe : Guerrier" << std::endl;
				}
				if (IsChara<CArcher>(*(Liste->listeCharacter[i])) == true)
				{
					std::cout << "Classe : Archer" << std::endl;
				}
				if (IsChara<CRogue>(*(Liste->listeCharacter[i])) == true)
				{
					std::cout << "Classe : Voleur" << std::endl;
				}
				std::cout << "HP : " << Liste->listeCharacter[i]->getVie() << std::endl;
				std::cout << "Attaque : " << Liste->listeCharacter[i]->getAttaque() << std::endl;
				std::cout << "Agilite : " << Liste->listeCharacter[i]->getAgilite() << std::endl;
				std::cout << "Defense : " << Liste->listeCharacter[i]->getDefense() << std::endl;
				std::cout << "Esquive : " << Liste->listeCharacter[i]->getEsquive() << std::endl;
				std::cout << "Intellignece : " << Liste->listeCharacter[i]->getIntelligence() << std::endl;
				std::cout << "Vitesse : " << Liste->listeCharacter[i]->getVitesse() << "\n\n\n";
			}
			std::cout << "Choix troisi�me personnage : ";
		}
		else
		{
			std::cout << "Le choix n'est pas dans la liste des personnage !\n";
			std::cout << "Choix troisi�me personnage : ";
		}
	}
}

void addWeaponTeam(CInventory* Larme, CTeam* t)
{
	system("cls");
	int i, j, nbInv, choix;
	for (i = 0; i < 3; i++)
	{
		std::cout << "Ajout de l'arme pour " << t->c[i]->getNom() << std::endl;
		if (IsChara<CMage>(*(t->c[i])) == true)
		{
			std::cout << "Classe : Mage" << std::endl;
			std::cout << "Cette classe ne peut utiliser que les �p�es et les batons" << std::endl;
		}
		else if (IsChara<CWarrior>(*(t->c[i])) == true)
		{
			std::cout << "Classe : Guerrier" << std::endl;
			std::cout << "Cette classe ne peut utiliser que les �p�es et les dagues" << std::endl;
		}
		else if (IsChara<CArcher>(*(t->c[i])) == true)
		{
			std::cout << "Classe : Archer" << std::endl;
			std::cout << "Cette classe ne peut utiliser que les arcs et les dagues" << std::endl;
		}
		else if (IsChara<CRogue>(*(t->c[i])) == true)
		{
			std::cout << "Classe : Voleur" << std::endl;
			std::cout << "Cette classe ne peut utiliser que les dagues et les arcs" << std::endl;
		}
		std::cout << "Menu d'ajout des armes :\n";
		nbInv = Larme->listeArme.size();
		for (j = 0; j < nbInv; j++)
		{
			std::cout << j << " - " << Larme->listeArme[j]->getNom() << '\n';
		}
		std::cout << j << " - " << "Afficher stats armes\n";
		while (std::cin >> choix)
		{
			if (choix < 0 || choix > 8)
			{
				std::cout << "Ce choix est impossible !\n";
			}
			else
			{
				if (choix == 8)
				{
					for (j = 0; j < nbInv; j++)
					{
						std::cout << j << " - " << Larme->listeArme[j]->getNom() << '\n';
						std::cout << "L'agilite bonus de l'arme est " << Larme->listeArme[i]->getAgi() << '\n';
						std::cout << "Les degats de base de l'arme sont de " << Larme->listeArme[i]->getDegat() << '\n';
						std::cout << "L'intelligence bonus de l'arme est " << Larme->listeArme[i]->getIntel() << '\n';
						std::cout << "Les HP bonus de l'arme est " << Larme->listeArme[i]->getHP() << '\n';
						std::cout << "L'attaque bonus de l'arme est " << Larme->listeArme[i]->getAtk() << '\n';
						std::cout << "Les critiques de base de l'arme sont de " << Larme->listeArme[i]->getCrit() << '\n';
						std::cout << "La defense bonus de l'arme est " << Larme->listeArme[i]->getDef() << '\n';
						if (Is<CSword>(*(Larme->listeArme[i])) == true)
						{
							std::cout << "L'arme n'est compatible qu'avec les Guerriers et les Mages\n";
						}
						else if (Is<CDagger>(*(Larme->listeArme[i])) == true)
						{
							std::cout << "L'arme n'est compatible qu'avec les Archers et les Voleurs\n";
						}
						else if (Is<CStaff>(*(Larme->listeArme[i])) == true)
						{
							std::cout << "L'arme n'est compatible qu'avec les Mages\n";
						}
						else if (Is<CBow>(*(Larme->listeArme[i])) == true)
						{
							std::cout << "L'arme n'est compatible qu'avec les Archers et les Voleurs\n";
						}
						std::cout << '\n';
						std::cout << '\n';
					}
				}
				else
				{
					if (t->c[i]->isCompatible(Larme->listeArme[choix]))
					{
						t->c[i]->setWeapon(Larme->listeArme[choix]->copyw());
						break;
					}
					else
					{
						std::cout << "Cette arme n'est pas comptabile avec " << t->c[i]->getNom() << " !\n";
					}
				}
			}
		}
	}

}