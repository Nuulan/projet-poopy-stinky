#include "lectureFichier.h"
#include <string>

void getArme(const char* path,CInventory * liste)
{
	std::string arme = "";
	std::string nom = "";
	short degat = 0;
	float crit = 0;
	short intel = 0;
	short atk = 0;
	short HP = 0;
	short def = 0;
	short agi = 0;
	short dur = 0;
	short nbFleche = 0;
	short conso = 0;
	short t = 0;
	std::string line;
	std::ifstream Farme(path);
	std::string type = "";
	std::string arg = "";
	int i = 0;
	if (Farme.is_open())
	{
		std::cout << "Le fichier est ouvert" << '\n';
		while (getline(Farme, line))
		{
			if (line == "Weapon")
			{

			}
			else if (line == "EndWeapon")
			{
				if (arme == "Epee")
				{
					liste->listeArme.push_back(new CSword(nom, degat, crit, dur, intel, atk, HP, def, agi));
				}
				if (arme == "Baton")
				{
					liste->listeArme.push_back(new CStaff(nom, degat, crit, conso, intel, atk, HP, def, agi));
				}
				if (arme == "Dague")
				{
					liste->listeArme.push_back(new CDagger(nom, degat, crit, dur, intel, atk, HP, def, agi));
				}
				if (arme == "Arc")
				{
					liste->listeArme.push_back(new CBow(nom, degat, crit, nbFleche, intel, atk, HP, def, agi));
				}
				nom = "";
				degat = 0;
				crit = 0;
				dur = 0;
				intel = 0;
				atk = 0;
				HP = 0;
				def = 0;
				agi = 0;
				nbFleche = 0;
				conso = 0;
			}
			else
			{
				if (line.find("Type") != std::string::npos)
				{
					type = line.substr(1, 4);
					arg = line.substr(8, 5);
					arme = arg;
					type = "";
					arg = "";
				}
				if (line.find("Nom") != std::string::npos)
				{
					type = line.substr(1, 3);
					arg = line.substr(8, 20);
					nom = arg;
					type = "";
					arg = "";
				}
				if (line.find("HP") != std::string::npos)
				{
					type = line.substr(1, 2);
					arg = line.substr(7, 20);
					HP = std::stoi(arg, nullptr);
					type = "";
					arg = "";
				}
				if (line.find("Cout") != std::string::npos)
				{
					type = line.substr(1, 4);
					arg = line.substr(7, 20);
					conso = std::stoi(arg, nullptr);
					type = "";
					arg = "";
				}
				if (line.find("Intelligence") != std::string::npos)
				{
					type = line.substr(1, 12);
					arg = line.substr(14, 1);
					intel = std::stoi(arg, nullptr);
					type = "";
					arg = "";
				}
				if (line.find("Critique") != std::string::npos)
				{
					type = line.substr(1, 8);
					arg = line.substr(11, 4);
					crit = std::stof(arg, nullptr);
					type = "";
					arg = "";
				}
				if (line.find("Degat") != std::string::npos)
				{
					type = line.substr(1, 5);
					arg = line.substr(7, 20);
					degat = std::stoi(arg, nullptr);
					type = "";
					arg = "";
				}
				if (line.find("Durabilite") != std::string::npos)
				{
					type = line.substr(1, 10);
					arg = line.substr(13, 20);
					dur = std::stoi(arg, nullptr);
					type = "";
					arg = "";
				}
				if (line.find("Fleche") != std::string::npos)
				{
					type = line.substr(1, 6);
					arg = line.substr(7, 20);
					nbFleche = std::stoi(arg, nullptr);
					type = "";
					arg = "";
				}
				if (line.find("Agilite") != std::string::npos)
				{
					type = line.substr(1, 7);
					arg = line.substr(8, 20);
					agi = std::stoi(arg, nullptr);
					type = "";
					arg = "";
				}
				if (line.find("Attack") != std::string::npos)
				{
					type = line.substr(1, 6);
					arg = line.substr(8, 20);
					atk = std::stoi(arg, nullptr);
					type = "";
					arg = "";
				}
			}
		}
		Farme.close();
	}
	else
	{
		std::cout << "Le fichier n'a pas pu etre ouvert\n";
	}
}

void getChara(const char* path, CLcharacter* liste)
{
	std::string classe = "";
	std::string nom = "";
	short vie = 0;
	float esquive = 0;
	short vitesse = 0;
	short attaque = 0;
	short defense = 0;
	short agilite = 0;
	short intelligence = 0;
	float special = 0;
	std::string line;
	std::ifstream Fchara(path);
	std::string type = "";
	std::string arg = "";
	if (Fchara.is_open())
	{
		while (getline(Fchara,line))
		{
			if (line == "")
			{

			}
			if (line == "EndCharacter")
			{
				if (classe == "Guerrier")
				{
					liste->listeCharacter.push_back(new CWarrior(nom, vie, NULL, esquive, vitesse, attaque, defense, agilite, intelligence, special));
				}
				if (classe == "Voleur")
				{
					liste->listeCharacter.push_back(new CRogue(nom, vie, NULL, esquive, vitesse, attaque, defense, agilite, intelligence, special));
				}
				if (classe == "Archer")
				{
					liste->listeCharacter.push_back(new CArcher(nom, vie, NULL, esquive, vitesse, attaque, defense, agilite, intelligence));
				}
				if (classe == "Mage")
				{
					liste->listeCharacter.push_back(new CMage(nom, vie, NULL, esquive, vitesse, attaque, defense, agilite, intelligence));
				}
				classe = "";
				nom = "";
				vie = 0;
				esquive = 0;
				vitesse = 0;
				attaque = 0;
				defense = 0;
				agilite = 0;
				intelligence = 0;
				special = 0;
			}
			else
			{
				if (line.find("Classe") != std::string::npos)
				{
					type = line.substr(1,6);
					arg = line.substr(10, 20);
					classe = arg;
					type = "";
					arg = "";
				}
				if (line.find("Nom") != std::string::npos)
				{
					type = line.substr(1, 3);
					arg = line.substr(8, 20);
					nom = arg;
					type = "";
					arg = "";
				}
				if (line.find("Vitesse") != std::string::npos)
				{
					type = line.substr(1, 7);
					arg = line.substr(11, 20);
					vitesse = std::stoi(arg,nullptr);
					type = "";
					arg = "";
				}
				if (line.find("Attaque") != std::string::npos)
				{
					type = line.substr(1, 7);
					arg = line.substr(11, 20);
					attaque = std::stoi(arg, nullptr);
					type = "";
					arg = "";
				}
				if (line.find("Intelligence") != std::string::npos)
				{
					type = line.substr(1, 12);
					arg = line.substr(14, 20);
					intelligence = std::stoi(arg, nullptr);
					type = "";
					arg = "";
				}
				if (line.find("HP") != std::string::npos)
				{
					type = line.substr(1, 2);
					arg = line.substr(7, 20);
					vie = std::stoi(arg, nullptr);
					type = "";
					arg = "";
				}
				if (line.find("Defense") != std::string::npos)
				{
					type = line.substr(1, 7);
					arg = line.substr(11, 20);
					defense = std::stoi(arg, nullptr);
					type = "";
					arg = "";
				}
				if (line.find("Dodge") != std::string::npos)
				{
					type = line.substr(1, 5);
					arg = line.substr(9, 20);
					esquive = std::stof(arg, nullptr);
					type = "";
					arg = "";
				}
				if (line.find("Agilite") != std::string::npos)
				{
					type = line.substr(1, 7);
					arg = line.substr(11, 20);
					agilite = std::stoi(arg, nullptr);
					type = "";
					arg = "";
				}
				if (line.find("Special") != std::string::npos)
				{
					type = line.substr(1, 7);
					arg = line.substr(11, 20);
					special = std::stof(arg, nullptr);
					type = "";
					arg = "";
				}
			}
		}
	}
	else
	{
		std::cout << "Le fichier des personnages n'a pas pu etre ouvert !" << '\n';
	}
}