#pragma once
#include "CMelee.h"

class CSword : public CMelee
{
private:
	CSword();
public:
	CSword(std::string name, short atk, float critique, short dur, short i, short a, short h, short d, short ag);
	~CSword();
	short getDurabilite();
	CWeapon* copyw();
};