#include "CWarrior.h"

CWarrior::CWarrior() : CCharacter()
{
	special = 0;
}
CWarrior::CWarrior(std::string n, short life, CWeapon* w, float e, short v, short a, short d, short agi, short i, float spe) : CCharacter(n, life, w, e, v, a, d, agi, i)
{
	special = spe;
}
CWarrior::~CWarrior()
{

}
void CWarrior::skill()
{
	cd = 4;
}
bool CWarrior::isCompatible(CWeapon* weapon)
{
	if (Is<CSword>(*(weapon)))
	{
		return true;
		esquive += special;
	}
	else if (Is<CDagger>(*(weapon)))
	{
		return true;
	}
	else
	{
		return false;
	}
}
CCharacter* CWarrior::copy()
{
	return new CWarrior(*this);
}
short CWarrior::getMana()
{
	return 0;
}
void CWarrior::showSkill()
{
	std::cout << "1 - Provocation" << std::endl;
	std::cout << "2 - Reparer" << std::endl;
}
void CWarrior::reducCD()
{
	int res;
	if (cd != 0)
	{
		cd--;
		if (cd == 3)
		{
			srand(time(NULL));
			res = rand() % 50;
			if (res < 50)
			{
				menace = false;
			}
		}
		if (cd == 2)
		{
			menace = false;
		}
	}
}
int CWarrior::useSkill(CTeam * t)
{
	int choix, i, souschoix;
	std::cout << "Quel skill vous voulez utiliser ?" << std::endl;
	showSkill();
	while (std::cin >> choix)
	{
		if (choix == 1)
		{
			if (cd != 0)
			{
				return 1;
			}
			menace = true;
			cd = 4;
			return 0;
		}
		if (choix == 2)
		{
			std::cout << "Quelle arme vous voulez reparer ?" << std::endl;
			for (i = 0; i < 3; i++)
			{
				std::cout << t->c[i]->getArme()->getNom() << " appartenant a " << t->c[i]->getNom() << std::endl;
			}
			while (std::cin >> souschoix)
			{
				if (souschoix == 0)
				{
					t->c[0]->getArme()->reparation();
				}
				if (souschoix == 1)
				{
					t->c[1]->getArme()->reparation();
				}
				if (souschoix == 2)
				{
					t->c[2]->getArme()->reparation();
				}
				else
				{
					std::cout << "L'input n'est pas bon !" << std::endl;
				}
			}
		}
		else
		{
			std::cout << "L'input n'est pas bon !" << std::endl;
		}
	}
}
void CWarrior::Battle(CCharacter* cible, int choix)
{
	int i;
	float esq, res;
	srand(time(NULL));
	if (choix == 0)
	{
		esq = (rand() % 100);
		esq = esq / 100;
		if (esq < cible->getEsquive())
		{
			std::cout << cible->getNom() << "a esquiver l'attaque !" << std::endl;
		}
		else
		{
			res = ((rand() % 21) + 95);
			res = res / 100;
			cible->setDamage((attaque*res*16) / cible->getDefense());
		}
	}
	else
	{
		esq = (rand() % 100);
		esq = esq / 100;
		if (esq < cible->getEsquive())
		{
			std::cout << cible->getNom() << "a esquiver l'attaque !" << std::endl;
		}
		else
		{
			res = ((rand() % 26) + 85);
			res = res / 100;
			cible->setDamage((7*res*(attaque + arme->getDegat())) / cible->getDefense());
		}
	}
}