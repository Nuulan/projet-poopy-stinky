#pragma once
#include "CDagger.h"
#include "CSword.h"
#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include "lectureFichier.h"
#include "CInventory.h"
#include "CBow.h"
#include "CTeam.h"
#include "CWarrior.h"
#include "CLcharacter.h"
#include "Team.h"

void AffGame(CTeam* t1, CTeam* t2);
short LoopGame(CTeam* t1, CTeam* t2);
void GameTurn(CTeam* t1, CTeam* t2);