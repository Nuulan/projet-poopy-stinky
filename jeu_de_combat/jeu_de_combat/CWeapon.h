#pragma once
#include <iostream>

class CWeapon
{
protected:
	std::string nom;
	short degat;
	float crit;
	short intel;
	short atk;
	short HP;
	short def;
	short agi;
	bool enchanted;
public:
	CWeapon();
	CWeapon(std::string name, short deg, float critique, short i, short a, short h, short d, short ag);
	~CWeapon();
	std::string getNom();
	virtual short getDegat();
	float getCrit();
	short getIntel();
	short getAtk();
	short getHP();
	short getDef();
	short getAgi();
	virtual short second() = 0;
	virtual CWeapon* copyw() = 0;
	bool isEnchant();
	void setEnchant(int etat);
	virtual void reparation() = 0;
};