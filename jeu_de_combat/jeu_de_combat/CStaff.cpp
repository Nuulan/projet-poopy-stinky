#include "CStaff.h"

CStaff::CStaff()
{
	consoMana = 0;
}
CStaff::CStaff(std::string name, short atk, float critique, short conso, short i, short a, short h, short d, short ag) : CRanged(name, atk, critique, i, a, h, d, ag)
{
	consoMana = conso;
}
CStaff::~CStaff()
{

}
short CStaff::getDegat(short mana)
{
	if (consoMana > mana)
	{
		return 0;
	}
	else
	{
		return degat;
	}
}
short CStaff::second()
{
	return consoMana;
}
CWeapon* CStaff::copyw()
{
	return new CStaff(*this);
}