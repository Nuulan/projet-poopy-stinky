#pragma once
#include "CWeapon.h"

class CRanged : public CWeapon
{
protected:
	CRanged();
public:
	CRanged(std::string name, short atk, float critique, short i, short a, short h, short d, short ag);
	~CRanged();
	virtual short getDegat();
	virtual short second() = 0;
	virtual CWeapon* copyw() = 0;
	void reparation();
};