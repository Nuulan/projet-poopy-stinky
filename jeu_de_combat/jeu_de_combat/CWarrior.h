#pragma once
#include "CCharacter.h"

class CWarrior : public CCharacter
{
protected:
	float special;
	CWarrior();
public:
	~CWarrior();
	CWarrior(std::string n, short life, CWeapon* w, float e, short v, short a, short d, short agi, short i, float spe);
	void skill();
	bool isCompatible(CWeapon* weapon);
	CCharacter* copy();
	short getMana();
	virtual void showSkill();
	void reducCD();
	int useSkill(CTeam* t);
	void Battle(CCharacter* cible, int choix);
};