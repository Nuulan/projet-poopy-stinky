#include "CBow.h"

CBow::CBow() : CRanged()
{
	nbFleche = 0;
}
CBow::CBow(std::string name, short atk, float critique, short fleche, short i, short a, short h, short d, short ag) : CRanged(name, atk, critique, i, a, h, d, ag)
{
	nbFleche = fleche;
}
void CBow::minusFleche()
{
	if (nbFleche <= 0)
	{
		nbFleche = 0;
	}
	else
	{
		nbFleche--;
	}
}
CBow::~CBow()
{

}
short CBow::getDegat()
{
	if (nbFleche < 0)
	{
		std::cout << "Il n'y a plus de fleche !" << '\n';
		return 0;
	}
	else
	{
		minusFleche();
		return degat;
	}
}
short CBow::second()
{
	return nbFleche;
}
CWeapon* CBow::copyw()
{
	return new CBow(*this);
}