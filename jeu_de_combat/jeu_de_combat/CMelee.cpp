#include "CMelee.h"
#include <time.h>
#include <iostream>

CMelee::CMelee()
{
	durabilite = 30;
}
CMelee::CMelee(std::string name, short atk, float critique,short dur, short i, short a, short h, short d, short ag) : CWeapon(name, atk, critique, i, a, h, d, ag)
{
	durabilite = dur;
}
CMelee::~CMelee()
{

}
void CMelee::reparation()
{
	if (durabilite <= 0)
	{
		durabilite = 1;
	}
	else
	{
		srand(time(NULL));
		short dur = (rand() % 13) + 3;
		durabilite += dur;
	}
}
short CMelee::getDurabilite()
{
	return durabilite;
}
short CMelee::getDegat()
{
	if (durabilite <= 0)
	{
		return degat / 10;
	}
	else
	{
		return degat;
	}
}
short CMelee::second()
{
	return durabilite;
}