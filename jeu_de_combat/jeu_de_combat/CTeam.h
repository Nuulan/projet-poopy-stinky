#pragma once
#include <vector>

class CCharacter;

class CTeam
{
public:
	std::vector<CCharacter*> c;
	CTeam();
	void set(CCharacter* chara);
	~CTeam();
};