#include "CWeapon.h"

CWeapon::CWeapon()
{
	nom = "ERREUR";
	degat = 0;
	crit = 0;
	intel = 0;
	atk = 0;
	HP = 0;
	def = 0;
	agi = 0;
	enchanted = false;
}
CWeapon::CWeapon(std::string name, short deg, float critique, short i, short a, short h, short d, short ag)
{
	nom = name;
	degat = deg;
	crit = critique;
	intel = i;
	atk = a;
	HP = h;
	def = d;
	agi = ag;
	enchanted = false;
}
CWeapon::~CWeapon()
{

}
std::string CWeapon::getNom()
{
	return nom;
}
short CWeapon::getDegat()
{
	if (enchanted == true)
	{
		return degat * 1, 33;
	}
	else
	{
		return degat;
	}
}
float CWeapon::getCrit()
{
	return crit;
}
short CWeapon::getAgi()
{
	return agi;
}
short CWeapon::getAtk()
{
	return atk;
}
short CWeapon::getDef()
{
	return def;
}
short CWeapon::getHP()
{
	return HP;
}
short CWeapon::getIntel()
{
	return intel;
}
bool CWeapon::isEnchant()
{
	return enchanted;
}
void CWeapon::setEnchant(int etat)
{
	if (etat == 0)
	{
		enchanted = false;
	}
	else
	{
		enchanted = true;
	}
}