#pragma once
#include <iostream>
#include "CCharacter.h"

template <class T>
bool IsChara(CCharacter& perso)
{
	try
	{
		dynamic_cast<T&>(perso);
		return true;
	}
	catch (const std::bad_cast)
	{
		return false;
	}
}