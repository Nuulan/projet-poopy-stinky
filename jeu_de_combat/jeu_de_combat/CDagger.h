#pragma once
#include "CMelee.h"

class CDagger : public CMelee
{
public:
	CDagger(std::string name, short atk, float critique, short dur, short i, short a, short h, short d, short ag);
	~CDagger();
	CWeapon* copyw();
};