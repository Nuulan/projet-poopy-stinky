#pragma once
#include "CWeapon.h"
#include "CBow.h"
#include "CDagger.h"
#include "CStaff.h"
#include "CSword.h"
#include "CMelee.h"
#include "CRanged.h"
#include "IsTest.h"
#include <time.h>
#include "CTeam.h"

class CCharacter
{
protected:
	std::string nom;
	short vie;
	CWeapon* arme;
	float esquive;
	short vitesse;
	short attaque;
	short defense;
	short agilite;
	short intelligence;
	short cd;
	bool menace;
	bool furtif;
	short vietot;
	bool poison;
	short vitbase;
	CCharacter();
public:
	CCharacter(std::string name, short life, CWeapon* weapon, float evade, short speed, short attack, short defence, short agility, short wisdom);
	~CCharacter();
	std::string getNom();
	CWeapon * getArme();
	short getVie();
	float getEsquive();
	short getVitesse();
	short getAttaque();
	short getDefense();
	short getAgilite();
	short getIntelligence();
	virtual void Battle(CCharacter* cible, int choix) = 0;
	void setDamage(short degat);
	virtual void skill();
	void setVie(short life);
	void setEsquive(float evade);
	void setVitesse(short speed);
	void setAttaque(short attack);
	void setDefense(short defence);
	void setAgilite(short agility);
	void setIntelligence(short wisdom);
	void setWeapon(CWeapon* weapon);
	virtual bool isCompatible(CWeapon* weapon);
	virtual CCharacter* copy() = 0;
	virtual short getMana() = 0;
	virtual void showSkill();
	bool isMenace();
	bool isFurtif();
	short getVietot();
	virtual void reducCD();
	virtual int useSkill(CTeam* t) = 0;
	bool isPoison();
	void setPoison(int etat);
	void checkStatus();
	void changeSpeed(short dim);
	void resetSpeed();
};