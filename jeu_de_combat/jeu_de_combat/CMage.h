#pragma once
#include "CCharacter.h"
#include "IsTest.h"

class CMage : public CCharacter
{
protected:
	short mana;
	short cd2;
	CMage();
public:
	~CMage();
	CMage(std::string n, short life, CWeapon* w, float e, short v, short a, short d, short agi, short i);
	void skill();
	bool isCompatible(CWeapon* weapon);
	CCharacter* copy();
	short getMana();
	virtual void showSkill();
	void reducCD();
	int useSkill(CTeam* t);
	void Battle(CCharacter* cible, int choix);
};