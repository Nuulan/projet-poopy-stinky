#pragma once
#include <iostream>
#include "CWeapon.h"

template <class T>
bool Is(CWeapon& arme)
{
	try
	{
		dynamic_cast<T&>(arme);
		return true;
	}
	catch (const std::bad_cast)
	{
		return false;
	}
}