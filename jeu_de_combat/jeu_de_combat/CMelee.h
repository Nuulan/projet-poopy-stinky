#pragma once
#include "CWeapon.h"
#include <iostream>

class CMelee : public CWeapon
{
protected:
	short durabilite;
	CMelee();
public:
	CMelee(std::string name,short atk,float critique,short dur, short i, short a, short h, short d, short ag);
	~CMelee();
	void reparation();
	short getDurabilite();
	short getDegat();
	short second();
	virtual CWeapon* copyw() = 0;
};