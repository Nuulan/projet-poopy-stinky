#include "CRogue.h"

CRogue::CRogue() : CCharacter()
{
	special = 0;
}
CRogue::CRogue(std::string n, short life, CWeapon* w, float e, short v, short a, short d, short agi, short i, float spe) : CCharacter(n, life, w, e, v, a, d, agi, i)
{
	special = spe;
}
CRogue::~CRogue()
{

}
void CRogue::skill()
{
	cd = 4;
}
bool CRogue::isCompatible(CWeapon* weapon)
{
	if (Is<CBow>(*(weapon)))
	{
		return true;
	}
	else if (Is<CDagger>(*(weapon)))
	{
		return true;
	}
	else
	{
		return false;
	}
}
CCharacter* CRogue::copy()
{
	return new CRogue(*this);
}
short CRogue::getMana()
{
	return 0;
}
void CRogue::showSkill()
{
	std::cout << "1 - Furtif" << std::endl;
}
void CRogue::reducCD()
{
	int res;
	if (cd != 0)
	{
		cd--;
		if (cd == 3)
		{
			srand(time(NULL));
			res = rand() % 50;
			if (res < 50)
			{
				furtif = false;
			}
		}
		if (cd == 2)
		{
			furtif = false;
		}
	}
}
int CRogue::useSkill(CTeam* t)
{
	if (cd != 0)
	{
		return 1;
	}
	furtif = true;
	cd = 4;
	return 0;
}
void CRogue::Battle(CCharacter* cible, int choix)
{
	float esq, res, chance;
	srand(time(NULL));
	if (choix == 0)
	{
		esq = (rand() % 100);
		esq = esq / 100;
		if (esq < cible->getEsquive())
		{
			std::cout << cible->getNom() << "a esquiver l'attaque !" << std::endl;
		}
		else
		{
			res = ((rand() % 21) + 95);
			res = res / 100;
			cible->setDamage((16*attaque*res) / cible->getDefense());
			chance = (rand() % 100) / 100;
			if (chance < special)
			{
				cible->setPoison(1);
			}
		}
	}
	else
	{
		esq = (rand() % 100);
		esq = esq / 100;
		if (esq < cible->getEsquive())
		{
			std::cout << cible->getNom() << "a esquiver l'attaque !" << std::endl;
		}
		else
		{
			res = ((rand() % 26) + 85);
			res = res / 100;
			cible->setDamage((7 * (agilite + arme->getDegat()) * res) / cible->getDefense());
		}
	}
}