#pragma once
#include "CRanged.h"

class CBow : public CRanged
{
protected:
	short nbFleche;
	CBow();
public:
	CBow(std::string name, short atk, float critique, short fleche, short i, short a, short h, short d, short ag);
	void minusFleche();
	~CBow();
	short getDegat();
	short second();
	CWeapon* copyw();
};