#include "CCharacter.h"
#include <time.h>
#include "CTeam.h"

CCharacter::CCharacter()
{
	nom = "NULL";
	arme = NULL;
	esquive = 0;
	vitesse = 0;
	attaque = 0;
	defense = 0;
	agilite = 0;
	intelligence = 0;
	menace = false;
	furtif = false;
	poison = false;
	vietot = vie;
	vitbase = vitesse;
}
CCharacter::CCharacter(std::string name, short life, CWeapon* weapon, float evade, short speed, short attack, short defence, short agility, short wisdom)
{
	nom = name;
	arme = weapon;
	esquive = evade;
	vitesse = speed;
	attaque = attack;
	defense = defence;
	agilite = agility;
	intelligence = wisdom;
	cd = 0;
	vie = life;
	menace = false;
	furtif = false;
	poison = false;
	vietot = vie;
	vitbase = vitesse;
}
CCharacter::~CCharacter()
{

}
short CCharacter::getAgilite()
{
	return agilite;
}
CWeapon * CCharacter::getArme()
{
	return arme;
}
short CCharacter::getAttaque()
{
	return attaque;
}
short CCharacter::getDefense()
{
	return defense;
}
float CCharacter::getEsquive()
{
	return esquive;
}
short CCharacter::getIntelligence()
{
	return intelligence;
}
std::string CCharacter::getNom()
{
	return nom;
}
short CCharacter::getVie()
{
	return vie;
}
short CCharacter::getVitesse()
{
	return vitesse;
}
void CCharacter::Battle(CCharacter* cible, int choix)
{

}
void CCharacter::setDamage(short degat)
{
	vie -= degat;
	if (vie < 0)
	{
		vie = 0;
	}
	if (vie > vietot)
	{
		vie = vietot;
	}
}
void CCharacter::skill()
{

}
void CCharacter::setAgilite(short agility)
{
	agilite = agility;
}
void CCharacter::setAttaque(short attack)
{
	attaque = attack;
}
void CCharacter::setDefense(short defence)
{
	defense = defence;
}
void CCharacter::setEsquive(float evade)
{
	esquive = evade;
}
void CCharacter::setIntelligence(short wisdom)
{
	intelligence = wisdom;
}
void CCharacter::setVie(short life)
{
	vie = life;
}
void CCharacter::setVitesse(short speed)
{
	vitesse = speed;
}
void CCharacter::setWeapon(CWeapon* weapon)
{
	if (!arme)
	{
		agilite += weapon->getAgi();
		attaque += weapon->getAtk();
		defense += weapon->getDef();
		vie += weapon->getHP();
		intelligence += weapon->getIntel();
		vietot = vie;
		arme = weapon;
	}
	else
	{
		agilite -= arme->getAgi();
		attaque -= arme->getAtk();
		defense -= arme->getDef();
		vie -= arme->getHP();
		intelligence -= arme->getIntel();
		vietot = vie;
		arme = weapon;
	}
}
bool CCharacter::isCompatible(CWeapon* weapon)
{
	return false;
}
void CCharacter::showSkill()
{

}
bool CCharacter::isMenace()
{
	return menace;
}
bool CCharacter::isFurtif()
{
	return furtif;
}
short CCharacter::getVietot()
{
	return vietot;
}
void CCharacter::reducCD()
{
	if (cd != 0)
	{
		cd--;
	}
}
bool CCharacter::isPoison()
{
	return poison;
}
void CCharacter::setPoison(int etat)
{
	if (etat == 0)
	{
		poison = false;
	}
	if (etat == 1)
	{
		poison = true;
	}
}
void CCharacter::checkStatus()
{
	if (poison == true)
	{
		setDamage(vietot / 12);
	}
}
void CCharacter::changeSpeed(short dim)
{
	vitesse -= dim;
}
void CCharacter::resetSpeed()
{
	vitesse = vitbase;
}