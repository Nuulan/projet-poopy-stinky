#pragma once
#include "CDagger.h"
#include "CSword.h"
#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include "lectureFichier.h"
#include "CInventory.h"
#include "CBow.h"
#include "CTeam.h"
#include "CWarrior.h"
#include "CLcharacter.h"
#include "IsTest.h"
#include "IsTestChara.h"

void createTeam(CTeam * t, CLcharacter * Liste);
void addWeaponTeam(CInventory* Larme, CTeam* t);