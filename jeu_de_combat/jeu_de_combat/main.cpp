#include "CDagger.h"
#include "CSword.h"
#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include "lectureFichier.h"
#include "CInventory.h"
#include "CBow.h"
#include "CTeam.h"
#include "CWarrior.h"
#include "CLcharacter.h"
#include "Team.h"
#include "game.h"

int main(int argc, char* argv)
{
	int nbInv, i;
	CInventory Larme = CInventory();
	CLcharacter Lchara = CLcharacter();
	CTeam t1 = CTeam();
	CTeam t2 = CTeam();
	getArme("..\\fichier jdc\\weapons.jdc", &Larme);
	getChara("..\\fichier jdc\\characters.jdc", &Lchara);
	createTeam(&t1,&Lchara);
	createTeam(&t2, &Lchara);
	addWeaponTeam(&Larme, &t1);
	addWeaponTeam(&Larme, &t2);
	while (LoopGame(&t1,&t2) == 0)
	{
		GameTurn(&t1,&t2);
		std::cout << "FIn du tour !" << std::endl;
		system("pause");
	}
	AffGame(&t1, &t2);
	if (LoopGame(&t1, &t2) == 1)
	{
		std::cout << "Le joueur 1 a gagne !\n";
	}
	if (LoopGame(&t1,&t2) == 2)
	{
		std::cout << "Le joueur 2 a gagne !\n";
	}
	system("pause");
	return 0;
}