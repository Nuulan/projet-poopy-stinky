#pragma once
#include "CRanged.h"

class CStaff : public CRanged
{
protected:
	short consoMana;
	CStaff();
public:
	CStaff(std::string name, short atk, float critique, short conso, short i, short a, short h, short d, short ag);
	~CStaff();
	short getDegat(short mana);
	short second();
	CWeapon* copyw();
};