#include "CArcher.h"

CArcher::CArcher() : CCharacter()
{
	viser = false;
}
CArcher::CArcher(std::string n, short life, CWeapon* w, float e, short v, short a, short d, short agi, short i) : CCharacter(n, life, w, e, v, a, d, agi, i)
{
	viser = false;
}
CArcher::~CArcher()
{

}
void CArcher::skill()
{
	cd = 4;
}
bool CArcher::isCompatible(CWeapon * weapon)
{
	if (Is<CBow>(*(weapon)))
	{
		return true;
	}
	else if (Is<CDagger>(*(weapon)))
	{
		return true;
	}
	else
	{
		return false;
	}
}
CCharacter* CArcher::copy()
{
	return new CArcher(*this);
}
short CArcher::getMana()
{
	return 0;
}
void CArcher::showSkill()
{
	std::cout << "1 - Guerir Poison" << std::endl;
	std::cout << "2 - Viser" << std::endl;
}
void CArcher::reducCD()
{
	if (cd != 0)
	{
		cd--;
	}
}
int CArcher::useSkill(CTeam* t)
{
	int i, choix, prechoix;
	std::cout << "Quelle skill voulez-vous utiliser ?" << std::endl;
	showSkill();
	while (std::cin >> prechoix)
	{
		if (prechoix == 1)
		{
			if (cd != 0)
			{
				return 1;
			}
			else
			{
				std::cout << "Sur qui voulez-vous jeter la potion de guerison de poison ?" << std::endl;
				for (i = 0; i < 3; i++)
				{
					if (t->c[i]->getVie() != 0)
					{
						std::cout << i << " - " << t->c[i]->getNom() << std::endl;
					}
				}
				while (std::cin >> choix)
				{
					if (choix < 3 && choix >= 0)
					{
						std::cout << "Vous jetez la potion sur " << t->c[choix]->getNom() << std::endl;
						t->c[choix]->setPoison(0);
						cd = 3;
						return 0;
					}
					else
					{
						std::cout << "L'input n'est pas bon !" << std::endl;
						std::cout << "Sur qui voulez-vous jeter la potion de guerison de poison ?" << std::endl;
						for (i = 0; i < 3; i++)
						{
							if (t->c[i]->getVie() != 0)
							{
								std::cout << i << " - " << t->c[i]->getNom() << std::endl;
							}
						}
					}
				}
			}
		}
		if (prechoix == 2)
		{
			if (Is<CBow>(*arme) == true)
			{
				viser = true;
				std::cout << getNom() << " a commencer a viser" << std::endl;
			}
			else
			{
				std::cout << "Tu ne poss�des pas d'arc tu ne peux pas viser" << std::endl;
			}
		}
		else
		{
			std::cout << "L'input n'est pas bon !" << std::endl;
		}
	}
}
void CArcher::Battle(CCharacter* cible, int choix)
{
	int i;
	float esq, res;
	srand(time(NULL));
	if (choix == 0)
	{
		esq = (rand() % 100);
		esq = esq / 100;
		if (esq < cible->getEsquive())
		{
			std::cout << cible->getNom() << "a esquiver l'attaque !" << std::endl;
		}
		else
		{
			res = ((rand() % 21) + 95);
			res = res / 100;
			cible->setDamage((16*res*attaque) / cible->getDefense());
		}
	}
	else
	{
		esq = (rand() % 100);
		esq = esq / 100;
		if (esq < cible->getEsquive())
		{
			std::cout << cible->getNom() << "a esquiver l'attaque !" << std::endl;
		}
		else
		{
			res = ((rand() % 26) + 85);
			res = res / 100;
			if (viser == true)
			{
				cible->setDamage((9 * (agilite*1.33 + arme->getDegat()) * res) / cible->getDefense());
			}
			else
			{
				cible->setDamage(((9 * (agilite + arme->getDegat()) * res) / cible->getDefense()));
			}
		}
	}
}