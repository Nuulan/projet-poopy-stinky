#include "game.h"

void AffGame(CTeam* t1, CTeam* t2)
{
	system("cls");
	int i;
	std::cout << "Joueur 1\n";
	std::cout << t1->c[0]->getNom() << "\t\t\t" << t1->c[1]->getNom() << "\t\t\t" << t1->c[2]->getNom() << std::endl;
	//std::cout << t1->c[0]->getArme()->getNom() << "\t" << t1->c[1]->getArme()->getNom() << "\t" << t1->c[2]->getArme()->getNom() << std::endl;
	std::cout << t1->c[0]->getVie() << " / " << t1->c[0]->getVietot() << "\t\t\t" << t1->c[1]->getVie() << " / " << t1->c[1]->getVietot() << "\t\t\t" << t1->c[2]->getVie() << " / " << t1->c[2]->getVietot() << std::endl;
	for (i = 0; i < 3; i++)
	{
		if (IsChara<CMage>(*(t1->c[i])) && Is<CStaff>(*(t1->c[i]->getArme())))
		{
			std::cout << t1->c[i]->getMana();
		}
		else
		{
			std::cout << t1->c[i]->getArme()->second();;
		}
		if (i < 2)
		{
			std::cout << "\t\t\t";
		}
	}
	for (i = 0; i < 5; i++)
	{
		std::cout << std::endl;
	}
	std::cout << "Joueur 2\n";
	std::cout << t2->c[0]->getNom() << "\t\t\t" << t2->c[1]->getNom() << "\t\t\t" << t2->c[2]->getNom() << std::endl;
	//std::cout << t2->c[0]->getArme()->getNom() << "\t" << t2->c[1]->getArme()->getNom() << "\t" << t2->c[2]->getArme()->getNom() << std::endl;
	std::cout << t2->c[0]->getVie() << " / " << t2->c[0]->getVietot() << "\t\t" << t2->c[1]->getVie() << " / " << t2->c[1]->getVietot() << "\t\t" << t2->c[2]->getVie() << " / " << t2->c[2]->getVietot() << std::endl;
	for (i = 0; i < 3; i++)
	{
		if (IsChara<CMage>(*(t2->c[i])) && Is<CStaff>(*(t2->c[i]->getArme())))
		{
			std::cout << t2->c[i]->getMana();
		}
		else
		{
			std::cout << t2->c[i]->getArme()->second();;
		}
		if (i < 2)
		{
			std::cout << "\t\t\t";
		}
	}
	std::cout << std::endl;
}

short LoopGame(CTeam* t1, CTeam* t2)
{
	int i, t1a = 0, t2a = 0;
	for (i = 0; i < 3; i++)
	{
		if (t1->c[i]->getVie() <= 0)
		{
			t1->c[i]->setVitesse(999);
			t1a++;
		}
		if (t2->c[i]->getVie() <= 0)
		{
			t2->c[i]->setVitesse(999);
			t2a++;
		}
	}
	if (t1a == 3)
	{
		return 2;
	}
	if (t2a == 3)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

void GameTurn(CTeam* t1, CTeam* t2)
{
	int i, j, t1s, t2s, choix, souschoix, cible, attack = 0, nbCible = 0;
	if (t1->c[0]->getVitesse() < t1->c[1]->getVitesse())
	{
		t1s = 0;
		if (t1->c[0]->getVitesse() < t1->c[2]->getVitesse())
		{

		}
		else
		{
			t1s = 2;
		}
	}
	else
	{
		t1s = 1;
		if (t1->c[1]->getVitesse() < t1->c[2]->getVitesse())
		{

		}
		else
		{
			t1s = 2;
		}
	}
	if (t2->c[0]->getVitesse() < t2->c[1]->getVitesse())
	{
		t2s = 0;
		if (t2->c[0]->getVitesse() < t2->c[2]->getVitesse())
		{

		}
		else
		{
			t2s = 2;
		}
	}
	else
	{
		t2s = 1;
		if (t2->c[1]->getVitesse() < t2->c[2]->getVitesse())
		{

		}
		else
		{
			t2s = 2;
		}
	}
	if (t1->c[t1s]->getVitesse() < t2->c[t2s]->getVitesse())
	{
		t1->c[t1s]->checkStatus();
		if (t1->c[t1s]->getVie() == 0)
		{
			std::cout << t1->c[t1s]->getNom() << " est mort !" << std::endl;
			AffGame(t1, t2);
			return;
		}
		AffGame(t1, t2);
		for (i = 0; i < 3; i++)
		{
			t1->c[i]->changeSpeed(t1->c[t1s]->getVitesse());
			t2->c[i]->changeSpeed(t1->c[t1s]->getVitesse());
		}
		t1->c[t1s]->resetSpeed();
		std::cout << "C'est au tour du joueur 1 avec " << t1->c[t1s]->getNom() << std::endl;
		std::cout << "Que voulez-vous faire ?" << std::endl << "1 - Taper a main nue" << std::endl
			<< "2 - Taper avec " << t1->c[t1s]->getArme()->getNom() << std::endl << "3 - Skills" << std::endl;
		t1->c[t1s]->reducCD();
		while (std::cin >> choix)
		{
			if (choix == 1)
			{
				std::cout << "Est-vous sur de vouloir attaquer a main nue ? ( 1 - oui, 2 - non )" << std::endl;
				while (std::cin >> souschoix)
				{
					if (souschoix == 1)
					{
						std::cout << "Qui voulez-vous attaquer ?" << std::endl;
						for (i = 0; i < 3; i++)
						{
							if (t2->c[i]->getVie() != 0)
							{
								if (t2->c[i]->isMenace() == true)
								{
									std::cout << t2->c[i]->getNom() << " est en train de provoquer vous allez l'attaquer !" << std::endl;
									t1->c[t1s]->Battle(t2->c[i], 0);
									AffGame(t1, t2);
									return;
								}
								if (t2->c[i]->isFurtif() == false)
								{
									std::cout << nbCible << " - " << t2->c[nbCible]->getNom() << " PDV :" << t2->c[nbCible]->getVie() << std::endl;
								}
							}
							nbCible++;
						}
						while (std::cin >> cible)
						{
							if (cible >= 0 && cible < nbCible)
							{
								t1->c[t1s]->Battle(t2->c[cible], 0);
								AffGame(t1, t2);
								return;
							}
							else
							{
								nbCible = 0;              
								std::cout << "Cet input n'est pas bon !";
								std::cout << "Qui voulez-vous attaquer ?" << std::endl;
								for (i = 0; i < 3; i++)
								{
									if (t2->c[i]->getVie() != 0)
									{
										if (t2->c[i]->isFurtif() == false)
										{
											std::cout << nbCible << " - " << t2->c[nbCible]->getNom() << " PDV :" << t2->c[nbCible]->getVie() << std::endl;
										}
									}
									nbCible++;
								}
							}
						}
					nbCible = 0;
					}
					if (souschoix == 2)
					{
						std::cout << "Que voulez-vous faire ?" << std::endl << "1 - Taper a main nue" << std::endl
							<< "2 - Taper avec " << t1->c[t1s]->getArme()->getNom() << std::endl << "3 - Skills" << std::endl;
						break;
					}
					else
					{
						std::cout << "L'input n'est pas bon !" << std::endl;
						std::cout << "Que voulez-vous faire ?" << std::endl << "1 - Taper a main nue" << std::endl
							<< "2 - Taper avec " << t1->c[t1s]->getArme()->getNom() << std::endl << "3 - Skills" << std::endl;
						break;
					}
				}
			}
			if (choix == 2)
			{
				std::cout << "Est-vous sur de vouloir attaquer avec " << t1->c[t1s]->getArme()->getNom() << "? ( 1 - oui, 2 - non )" << std::endl;
				while (std::cin >> souschoix)
				{
					if (souschoix == 1)
					{
						std::cout << "Qui voulez-vous attaquer ?" << std::endl;
						for (i = 0; i < 3; i++)
						{
							if (t2->c[i]->getVie() != 0)
							{
								if (t2->c[i]->isMenace() == true)
								{
									std::cout << t2->c[i]->getNom() << " est en train de provoquer vous allez l'attaquer !" << std::endl;
									t1->c[t1s]->Battle(t2->c[i], 0);
									AffGame(t1, t2);
									return;
								}
								if (t2->c[i]->isFurtif() == false)
								{
									std::cout << nbCible << " - " << t2->c[nbCible]->getNom() << " PDV :" << t2->c[nbCible]->getVie() << std::endl;
								}
							}
							nbCible++;
						}
						while (std::cin >> cible)
						{
							if (cible >= 0 && cible < nbCible)
							{
								t1->c[t1s]->Battle(t2->c[cible], 1);
								AffGame(t1, t2);
								return;
							}
							else
							{
								std::cout << "Cet input n'est pas bon !";
								std::cout << "Qui voulez-vous attaquer ?" << std::endl;
								for (i = 0; i < 3; i++)
								{
									if (t2->c[i]->getVie() != 0)
									{
										if (t2->c[i]->isFurtif() == false)
										{
											std::cout << nbCible << " - " << t2->c[nbCible]->getNom() << " PDV :" << t2->c[nbCible]->getVie() << std::endl;
										}
									}
									nbCible++;
								}
							}
						}
					nbCible = 0;
					}
					if (souschoix == 2)
					{
						std::cout << "Que voulez-vous faire ?" << std::endl << "1 - Taper a main nue" << std::endl
							<< "2 - Taper avec " << t1->c[t1s]->getArme()->getNom() << std::endl << "3 - Skills" << std::endl;
						break;
					}
					else
					{
						std::cout << "L'input n'est pas bon !" << std::endl;
						std::cout << "Que voulez-vous faire ?" << std::endl << "1 - Taper a main nue" << std::endl
							<< "2 - Taper avec " << t1->c[t1s]->getArme()->getNom() << std::endl << "3 - Skills" << std::endl;
						break;
					}
				}
			}
			if (choix == 3)
			{
				std::cout << "Etes-vous sur de vouloir faire un skill ? ( 1 - Oui , 2 - Non )" << std::endl;
				while (std::cin >> souschoix)
				{
					if (souschoix == 1)
					{
						if (t1->c[t1s]->useSkill(t2) == 0)
						{
							AffGame(t1, t2);
							return;
						}
						else
						{
							std::cout << "Le skill est en cooldown !" << std::endl;
							std::cout << "Que voulez-vous faire ?" << std::endl << "1 - Taper a main nue" << std::endl
								<< "2 - Taper avec " << t1->c[t1s]->getArme()->getNom() << std::endl << "3 - Skills" << std::endl;
							break;
						}
					}
					if (souschoix == 2)
					{
						std::cout << "Que voulez-vous faire ?" << std::endl << "1 - Taper a main nue" << std::endl
							<< "2 - Taper avec " << t1->c[t1s]->getArme()->getNom() << std::endl << "3 - Skills" << std::endl;
						break;
					}
					if (souschoix < 1 || souschoix > 2)
					{
						std::cout << "L'input n'est pas bon !" << std::endl;
						std::cout << "Que voulez-vous faire ?" << std::endl << "1 - Taper a main nue" << std::endl
							<< "2 - Taper avec " << t1->c[t1s]->getArme()->getNom() << std::endl << "3 - Skills" << std::endl;
						break;
					}
				}
			}
			if (choix < 0 || choix > 3)
			{
				std::cout << "Cet input n'est pas bon !" << std::endl;
			}
		}
	}
	else
	{
		std::cout << "C'est au tour du joueur 2 avec " << t2->c[t2s]->getNom() << std::endl;
		t2->c[t2s]->checkStatus();
		if (t2->c[t2s]->getVie() == 0)
		{
			std::cout << t2->c[t2s]->getNom() << " est mort !" << std::endl;
			AffGame(t1, t2);
			return;
		}
		AffGame(t1, t2);
		for (i = 0; i < 3; i++)
		{
			t1->c[i]->changeSpeed(t2->c[t2s]->getVitesse());
			t2->c[i]->changeSpeed(t2->c[t2s]->getVitesse());
		}
		t2->c[t2s]->resetSpeed();
		std::cout << "C'est au tour du joueur 2 avec " << t2->c[t2s]->getNom() << std::endl;
		std::cout << "Que voulez-vous faire ?" << std::endl << "1 - Taper a main nue" << std::endl
			<< "2 - Taper avec " << t2->c[t2s]->getArme()->getNom() << std::endl << "3 - Skills" << std::endl;
		t2->c[t2s]->reducCD();
		while (std::cin >> choix)
		{
			if (choix == 1)
			{
				std::cout << "Est-vous sur de vouloir attaquer a main nue ? ( 1 - oui, 2 - non )" << std::endl;
				while (std::cin >> souschoix)
				{
					if (souschoix == 1)
					{
						std::cout << "Qui voulez-vous attaquer ?" << std::endl;
						for (i = 0; i < 3; i++)
						{
							if (t1->c[i]->getVie() != 0)
							{
								if (t1->c[i]->isMenace() == true)
								{
									std::cout << t1->c[i]->getNom() << " est en train de provoquer vous allez l'attaquer !" << std::endl;
									t2->c[t2s]->Battle(t1->c[i], 0);
									AffGame(t1, t2);
									return;
								}
								if (t2->c[i]->isFurtif() == false)
								{
									std::cout << nbCible << " - " << t1->c[nbCible]->getNom() << " PDV :" << t1->c[nbCible]->getVie() << std::endl;
								}
							}
							nbCible++;
						}
						while (std::cin >> cible)
						{
							if (cible >= 0 && cible < nbCible)
							{
								t2->c[t2s]->Battle(t1->c[cible], 0);
								AffGame(t1, t2);
								return;
							}
							else
							{
								nbCible = 0;
								std::cout << "Cet input n'est pas bon !";
								std::cout << "Qui voulez-vous attaquer ?" << std::endl;
								for (i = 0; i < 3; i++)
								{
									if (t1->c[i]->getVie() != 0)
									{
										if (t1->c[i]->isFurtif() == false)
										{
											std::cout << nbCible << " - " << t1->c[nbCible]->getNom() << " PDV :" << t1->c[nbCible]->getVie() << std::endl;
										}
									}
									nbCible++;
								}
							}
						}
						nbCible = 0;
					}
					if (souschoix == 2)
					{
						std::cout << "Que voulez-vous faire ?" << std::endl << "1 - Taper a main nue" << std::endl
							<< "2 - Taper avec " << t2->c[t2s]->getArme()->getNom() << std::endl << "3 - Skills" << std::endl;
						break;
					}
					else
					{
						std::cout << "L'input n'est pas bon !" << std::endl;
						std::cout << "Que voulez-vous faire ?" << std::endl << "1 - Taper a main nue" << std::endl
							<< "2 - Taper avec " << t2->c[t2s]->getArme()->getNom() << std::endl << "3 - Skills" << std::endl;
						break;
					}
				}
			}
			if (choix == 2)
			{
				std::cout << "Est-vous sur de vouloir attaquer avec " << t2->c[t2s]->getArme()->getNom() << "? ( 1 - oui, 2 - non )" << std::endl;
				while (std::cin >> souschoix)
				{
					if (souschoix == 1)
					{
						std::cout << "Qui voulez-vous attaquer ?" << std::endl;
						for (i = 0; i < 3; i++)
						{
							if (t1->c[i]->getVie() != 0)
							{
								if (t1->c[i]->isMenace() == true)
								{
									std::cout << t1->c[i]->getNom() << " est en train de provoquer vous allez l'attaquer !" << std::endl;
									t2->c[t2s]->Battle(t1->c[i], 0);
									AffGame(t1, t2);
									return;
								}
								if (t1->c[i]->isFurtif() == false)
								{
									std::cout << nbCible << " - " << t1->c[nbCible]->getNom() << " PDV :" << t1->c[nbCible]->getVie() << std::endl;
								}
							}
							nbCible++;
						}
						while (std::cin >> cible)
						{
							if (cible >= 0 && cible < nbCible)
							{
								t2->c[t2s]->Battle(t1->c[cible], 1);
								AffGame(t1, t2);
								return;
							}
							else
							{
								std::cout << "Cet input n'est pas bon !";
								std::cout << "Qui voulez-vous attaquer ?" << std::endl;
								for (i = 0; i < 3; i++)
								{
									if (t1->c[i]->getVie() != 0)
									{
										if (t1->c[i]->isFurtif() == false)
										{
											std::cout << nbCible << " - " << t1->c[nbCible]->getNom() << " PDV :" << t1->c[nbCible]->getVie() << std::endl;
										}
									}
									nbCible++;
								}
							}
						}
						nbCible = 0;
					}
					if (souschoix == 2)
					{
						std::cout << "Que voulez-vous faire ?" << std::endl << "1 - Taper a main nue" << std::endl
							<< "2 - Taper avec " << t2->c[t2s]->getArme()->getNom() << std::endl << "3 - Skills" << std::endl;
						break;
					}
					else
					{
						std::cout << "L'input n'est pas bon !" << std::endl;
						std::cout << "Que voulez-vous faire ?" << std::endl << "1 - Taper a main nue" << std::endl
							<< "2 - Taper avec " << t2->c[t2s]->getArme()->getNom() << std::endl << "3 - Skills" << std::endl;
						break;
					}
				}
			}
			if (choix == 3)
			{
				std::cout << "Etes-vous sur de vouloir faire un skill ? ( 1 - Oui , 2 - Non )" << std::endl;
				while (std::cin >> souschoix)
				{
					if (souschoix == 1)
					{
						if (t2->c[t2s]->useSkill(t2) == 0)
						{
							AffGame(t1, t2);
							return;
						}
						else
						{
							std::cout << "Le skill est en cooldown !" << std::endl;
							std::cout << "Que voulez-vous faire ?" << std::endl << "1 - Taper a main nue" << std::endl
								<< "2 - Taper avec " << t2->c[t2s]->getArme()->getNom() << std::endl << "3 - Skills" << std::endl;
							break;
						}
					}
					if (souschoix == 2)
					{
						std::cout << "Que voulez-vous faire ?" << std::endl << "1 - Taper a main nue" << std::endl
							<< "2 - Taper avec " << t2->c[t2s]->getArme()->getNom() << std::endl << "3 - Skills" << std::endl;
						break;
					}
					if (souschoix < 1 ||souschoix > 2)
					{
						std::cout << "L'input n'est pas bon !" << std::endl;
						std::cout << "Que voulez-vous faire ?" << std::endl << "1 - Taper a main nue" << std::endl
							<< "2 - Taper avec " << t2->c[t2s]->getArme()->getNom() << std::endl << "3 - Skills" << std::endl;
						break;
					}
				}
			}
			if (choix < 0 || choix > 3)
			{
				std::cout << "Cet input n'est pas bon !" << std::endl;
			}
		}
	}
}
