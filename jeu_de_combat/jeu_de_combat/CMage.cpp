#include "CMage.h"

CMage::CMage() : CCharacter()
{
	mana = 0;
	cd2 = 0;
}
CMage::CMage(std::string n, short life, CWeapon* w, float e, short v, short a, short d, short agi, short i) : CCharacter(n, life, w, e, v, a, d, agi, i)
{
	mana = 10 * i;
	cd2 = 0;
}
CMage::~CMage()
{

}
void CMage::skill()
{
	cd = 4;
}
bool CMage::isCompatible(CWeapon* weapon)
{
	if (Is<CSword>(*(weapon)))
	{
		return true;
	}
	else if (Is<CStaff>(*(weapon)))
	{
		return true;
	}
	else
	{
		return false;
	}
}
CCharacter* CMage::copy()
{
	return new CMage(*this);
}
short CMage::getMana()
{
	return mana;
}
void CMage::showSkill()
{
	std::cout << "1 - Regeneration Mana" << std::endl;
	std::cout << "2 - Soigner" << std::endl;
	std::cout << "3 - Enchanter Arme" << std::endl;
}
void CMage::reducCD()
{
	if (cd != 0)
	{
		cd--;
	}
	if (cd2 != 2)
	{
		cd2--;
	}
}
int CMage::useSkill(CTeam* t)
{
	int i, choix, souschoix, res;
	std::cout << "Quel skill utiliser ?" << std::endl;
	showSkill();
	while (std::cin >> choix)
	{
		if (choix < 4 && choix > 0)
		{
			if (choix == 1)
			{
				srand(time(NULL));
				res = (rand() % 6) + 2;
				mana += res;
				std::cout << "Vous avez recuperer " << res << " de mana" << std::endl;
				return 0;
			}
			if (choix == 2)
			{
				if (cd != 0)
				{
					return 1;
				}
				if (Is<CStaff>(*arme) == true)
				{
					std::cout << "qui voulez-vous soigner ?" << std::endl;
					for (i = 0; i < 3; i++)
					{
						std::cout << i << " - " << t->c[i]->getNom() << " PDV : " << t->c[i]->getVie() << std::endl;
					}
					while (std::cin >> souschoix)
					{
						if (t->c[souschoix]->getVie() != 0)
						{
							srand(time(NULL));
							res = (rand() % 11) + 10;
							t->c[souschoix]->setDamage(-res);
							cd = 4;
							return 0;
						}
						else
						{
							std::cout << t->c[souschoix]->getNom() << "est deja mort ! Tu ne peux le soigner !" << std::endl;
							std::cout << "qui voulez-vous soigner ?" << std::endl;
							for (i = 0; i < 3; i++)
							{
								std::cout << t->c[i]->getNom() << " PDV : " << t->c[i]->getVie() << std::endl;
							}
						}
					}
				}
			}
			if (choix == 3)
			{
				if (cd2 != 0)
				{
					return 1;
				}
				std::cout << "Quelle arme voulez-vous enchanter ?" << std::endl;
				for (i = 0; i < 3; i++)
				{
					std::cout << i << " - " << t->c[i]->getArme()->getNom() << " de " << t->c[i]->getNom() << std::endl;
				}
				while (std::cin >> souschoix)
				{
					if (souschoix < 3 && souschoix >= 0)
					{
						t->c[i]->getArme()->setEnchant(1);
						cd2 = 3;
						return 0;
					}
					else
					{
						std::cout << "l'input n'est pas bon !" << std::endl;
					}
				}
			}
		}
		else
		{
			std::cout << "L'input n'est pas bon !" << std::endl;
		}
	}
}
void CMage::Battle(CCharacter* cible, int choix)
{
	int i;
	float esq, res;
	srand(time(NULL));
	if (choix == 0)
	{
		esq = (rand() % 100);
		esq = esq / 100;
		if (esq < cible->getEsquive())
		{
			std::cout << cible->getNom() << "a esquiver l'attaque !" << std::endl;
		}
		else
		{
			res = ((rand() % 21) + 95);
			res = res / 100;
			cible->setDamage((16*res*attaque) / cible->getDefense());
		}
	}
	else
	{
		esq = (rand() % 100);
		esq = esq / 100;
		if (esq < cible->getEsquive())
		{
			std::cout << cible->getNom() << "a esquiver l'attaque !" << std::endl;
		}
		else
		{
			if (Is<CStaff>(*arme) == true)
			{
				if (mana < arme->second())
				{
					std::cout << "Tu n'a pas asser de Mana !" << std::endl;
				}
				else
				{
					res = ((rand() % 26) + 85);
					res = res / 100;
					cible->setDamage((5*res*(intelligence + arme->getDegat())) / cible->getDefense());
					mana -= arme->second();
				}
			}
		}
	}
}