#pragma once
#define _CRT_SECURE_NO_WARNINGS
#include "CDagger.h"
#include "CSword.h"
#include "CBow.h"
#include "CStaff.h"
#include "CWarrior.h"
#include "CMage.h"
#include "CArcher.h"
#include "CRogue.h"
#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include "CInventory.h"
#include "CLcharacter.h"

void getArme(const char* path, CInventory * liste);
void getChara(const char* path, CLcharacter* liste);